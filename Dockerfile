# Use official Python's alpine image as base
FROM python:3.6-alpine3.7 AS builder

# Create BOT_PATH if it doesn't exist and set it as the working dir
ENV BOT_PATH=/usr/src/bot
WORKDIR $BOT_PATH

# Expose default port to connect with the service
EXPOSE 443

# Update and install packages
RUN apk add --update \
      build-base \
      postgresql-dev \
      python3-dev \
    && rm -rf /var/cache/apk/*

# Copy Gemfile and Gemfile.lock and install dependencies
COPY requirements.txt ./
RUN pip install -r requirements.txt

# Copy the application code to the installation path
COPY . .

# Define default command to execute when running the container
CMD [ "python", "bot.py" ]
