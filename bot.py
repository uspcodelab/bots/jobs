#!/usr/bin/env python

import telebot

import os
import logging
import datetime

from enum import Enum
from collections import defaultdict

################################################################################
##                           GLOBAL CONFIGURATIONS                            ##
################################################################################

# Bot configuration
token = os.environ['UCL_JOBS_TELEGRAM_TOKEN']
bot = telebot.TeleBot(token)

# Logging configuration
logger = telebot.logger
# logger.setLevel(logging.DEBUG) # Outputs debug messages to console.

################################################################################
##                                   FILTERS                                  ##
################################################################################

def is_doing_something_useful(message):
	chat_id = message.chat.id
	if chat_id in session:
		current_state = states[session[chat_id]["state"]]
		return False if (current_state == State.NONE) else True
	return False

################################################################################
##                             AUXILIARY FUNCTIONS                            ##
################################################################################

def none(message):
	pass

def set_job_title(message):
	print("set_job_title")
	session[message.chat.id]["job_title"] = message.text

def set_job_description(message):
	print("set_job_description")
	session[message.chat.id]["job_description"] = message.text

def end_job_registration(message):
	print("end_job_registration")
	chat_id = message.chat.id

	jobs.append({
		"title": session[chat_id]["job_title"],
		"description": session[chat_id]["job_description"],
		"date": datetime.datetime.now()
	})

	del session[chat_id]["job_title"]
	del session[chat_id]["job_description"]

################################################################################
##                                STATE MACHINE                               ##
################################################################################

# State machine
class State(Enum):
	NONE = 0
	GET_JOB_TITLE = 1
	GET_JOB_DESCRIPTION = 2
	END_JOB_REGISTRATION = 3

states = {
	State.NONE: {
		"next": State.NONE,
		"reply": "Digite algum dos comandos do bot!",
		"run": none
	},
	State.GET_JOB_TITLE: {
		"next": State.GET_JOB_DESCRIPTION,
		"reply": "Vamos começar! Primeiro, envie o título da vaga:",
		"run": set_job_title
	},
	State.GET_JOB_DESCRIPTION: {
		"next": State.END_JOB_REGISTRATION,
		"reply": "Ótimo! Agora, envie a descrição da vaga:",
		"run": set_job_description
	},
	State.END_JOB_REGISTRATION: {
		"next": State.NONE,
		"reply": "Obrigado! Sua vaga foi registrada :)",
		"run": end_job_registration
	}
}

################################################################################
##                                 GLOBAL DATA                                ##
################################################################################

# Session to store chat_id => current state
session = {}
jobs = []

################################################################################
##                              MESSAGE HANDLERS                              ##
################################################################################

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
	session[message.chat.id] = {}

	bot.reply_to(message,
			"Olá! Bem-vindo ao bot de registro de vagas do USPCodeLab")

@bot.message_handler(commands=['registrarVaga'])
def register_job(message):
	initial_state_id = State.GET_JOB_TITLE
	initial_state = states[initial_state_id]

	bot.send_message(message.chat.id, initial_state["reply"])

	session[message.chat.id] = { "state": initial_state_id }

@bot.message_handler(commands=['verVagas'])
def see_jobs(message):
	summary = ""
	for job in jobs:
		summary += "**" + job["title"] + "**" + "\n"
		summary += job["description"] + "\n"
		summary += "\n"

	bot.send_message(message.chat.id, summary, parse_mode="Markdown")

@bot.message_handler(func=is_doing_something_useful)
def answer_message(message):
	previous_state_id = session[message.chat.id]["state"]
	previous_state = states[previous_state_id]

	previous_state["run"](message)

	current_state_id = previous_state["next"]
	current_state = states[current_state_id]

	session[message.chat.id]["state"] = current_state_id
	bot.reply_to(message, current_state["reply"])

	if (current_state["next"] == State.NONE):
		current_state["run"](message)
		session[message.chat.id]["state"] = State.NONE

################################################################################
##                             BOT INITIALIZATION                             ##
################################################################################

bot.polling()
